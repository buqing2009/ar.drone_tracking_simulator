#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CATKIN_TEST_RESULTS_DIR="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/test_results"
export CMAKE_PREFIX_PATH="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/devel/lib:/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/devel/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH"
export PATH="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/devel/lib/pkgconfig:/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/devel/lib/x86_64-linux-gnu/pkgconfig:$PKG_CONFIG_PATH"
export PYTHONPATH="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy:/home/buqing2009/tum_simulator_ws/src:/home/buqing2009/kinect2_ws/src:/media/buqing2009/a1d34eb3-afde-40ab-ac2b-4afe21ee716f/buqing2009/kinect2_ws/src:/opt/ros/indigo/share:/opt/ros/indigo/stacks"
export ROS_TEST_RESULTS_DIR="/home/buqing2009/tum_simulator_ws/src/ardrone_helpers/joy/build/test_results"